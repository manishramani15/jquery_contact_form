function ContactManager() {
  this.emailREGEXP =  /^([A-Za-z0-9\-\.])+\@([A-Za-z0-9\-\.])+\.([A-Za-z]{2,})$/;
}

ContactManager.prototype.init = function() {
  this.createFields();
  this.bindFields();
  this.bindEvents();
};

ContactManager.prototype.createFields = function() {
  this.formContainer = $("<div>");
  this.form = $("<form>");
  this.nameContainer = $("<span>");
  this.nameInput = $("<input>");
  this.emailContainer = $("<span>");
  this.emailInput = $("<input>");
  this.addButton = $("<button>", {
                                  "type" : "button",
                                  "text" : "Add",
                                 });
  this.searchInput = $("<input>");
  this.searchContainer = $("<div>");
  this.contactContainer = $("<div>").css({
                                          "display" : "flex",
                                          "flex-wrap" : "wrap",
                                         });
};

ContactManager.prototype.bindFields = function() {
  this.nameContainer.append(" Name: ")
                    .append(this.nameInput);
  this.emailContainer.append(" Email ")
                     .append(this.emailInput);
  this.searchContainer.append(" Search: ")
                      .append(this.searchInput);
  this.form.append(this.nameContainer)
           .append(this.emailContainer)  
           .append(" ")
           .append(this.addButton)
           .append(this.searchContainer);
  this.formContainer.append(this.form)
                    .appendTo("body");
};

ContactManager.prototype.bindEvents = function() {
  var _this = this,
      nameValue = "",
      emailValue = "";
  this.addButton.on("click", function(event) {
    nameValue = _this.nameInput.val();
    emailValue = _this.emailInput.val();
    _this.isValid(nameValue, emailValue) ? _this.addContactInformationBox(nameValue, emailValue) : alert("check fields before adding");
  });
  this.searchInput.on("input", function() {
    var val = $(this).val();
    $('[data-behaviour="name"]').parent()
                                .hide();
    $('[data-behaviour="name"]:contains(' + val + ')').parent()
                                                      .show();
  });
};

ContactManager.prototype.addContactInformationBox = function(nameValue, emailValue) {
  var box = $('<div>'),
      nameTag = $("<span>", { "text" : "Name: " }),
      nameContent = $("<span>", { 
                                 "data-behaviour" : "name",
                                 "text" : nameValue
                                }),
      emailContent = $("<p>", { "text" : "Email: " + emailValue }),
      deleteButton = $("<button>", { 
                                    "type" : "button",
                                    "text" : "Delete"
                                   });
  box.append(nameTag)
     .append(nameContent)
     .append(emailContent)
     .append(deleteButton)
     .appendTo(this.contactContainer)
     .css({
           "margin" : "5px",
           "border" : "2px solid black",
           "width" : "170px",
           "height" : "120px",
          });
  this.nameInput.val("");
  this.emailInput.val("");
  deleteButton.on("click", function() {
    box.remove();
  });
  this.contactContainer.appendTo("body");
};

ContactManager.prototype.isValid = function(nameValue, emailValue) {
  return !(nameValue.trim() === "") && !(emailValue.trim() === "") && this.emailREGEXP.test(emailValue);
};

$(function() {
  var contactManager = new ContactManager();
  contactManager.init();
});